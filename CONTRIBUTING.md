# Contributing to Google Summer of Code

While everyone is welcome to contribute to [PRoot](https://github.com/proot-me/proot/blob/master/HACKING.rst),
the purpose of this repository is specifically for students in the Google Summer of Code (GSoC) program.

## Projects

Take a look at the [`ideas/`](https://github.com/proot-me/gsoc/tree/master/ideas) directory, for a list of project ideas that can be worked on, and find one that you like. If you have your own idea, [create an issue](https://github.com/proot-me/gsoc/issues/new?assignees=&labels=idea&template=project-idea.md&title=) for it to be added here. Once you have chosen an idea, then see the instructions below for submitting an official proposal.

## Proposals

Please take a look at the GSoC Student Guide on [writing a proposal](https://google.github.io/gsocguides/student/writing-a-proposal). Proposals should be submitted as a [pull request to this repository](https://github.com/proot-me/gsoc/pulls),
but only after submitting an [issue for a draft proposal](https://github.com/proot-me/gsoc/issues/new?assignees=&labels=proposal&template=student-proposal.md&title=). Once your proposal is ready, it will need to be submitted on the [GSoC website](https://summerofcode.withgoogle.com/dashboard) directly.
