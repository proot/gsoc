# [Google Summer of Code](https://summerofcode.withgoogle.com)

[![Developer Chat](https://img.shields.io/gitter/room/proot-me/devs.svg?style=flat-square)](https://gitter.im/proot-me/gsoc)
[![Google Calendar](https://img.shields.io/badge/calendar-google-yellow?style=flat-square)](https://calendar.google.com/calendar?cid=dTVwaWlrYzcyM3Zqa2NhZ2s1N25xZWdyMDBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
[![GSoC Status](https://img.shields.io/badge/gsoc--2020-org--rejected-red?style=flat-square)](https://user-images.githubusercontent.com/43783393/74989124-920f3680-540d-11ea-88b2-c4883303de69.png)

Welcome students! Please take a look at the [contributing guide](CONTRIBUTING.md) for more information.

## Credits

Inspired by [Gentoo's GSoC Project](https://wiki.gentoo.org/wiki/Google_Summer_of_Code).
Also borrows from the [Creative Commons Open Source website](https://github.com/creativecommons/creativecommons.github.io-source),
specifically their application instructions from GSoC 2019.

## License

[![Creative Commons License](http://i.creativecommons.org/l/by/4.0/88x31.png)][cc-by-4.0]

This work is licensed under a [Creative Commons Attribution 4.0 International License][cc-by-4.0].

[cc-by-4.0]: http://creativecommons.org/licenses/by/4.0
